<?php
namespace HIVE\HiveCfgRepository\UserFunc;
/***
 *
 * This file is part of the "hive_cfg_repository" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

class StorageUserFunc
{
    /**
     * function moved to \Hive\HiveUserfuncs
     * keep this  function for backward compatibility
     *
     * @param $sModel
     * @param $sPlugin
     * @return string
     */
    public static function getStoragePidQueryForModelInPlugin($sModel, $sPlugin)
    {
        return \Hive\HiveUserfuncs\UserFunc\StorageUserFunc::getStoragePidQueryForModelInPlugin($sModel, $sPlugin);
    }

    /**
     * function moved to \Hive\HiveUserfuncs
     * keep this function for backward compatibility
     *
     * @param $sModel
     * @param $sPlugin
     * @return string
     */
    public static function getStoragePidListForModelInPlugin($sModel, $sPlugin)
    {
        return \Hive\HiveUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sModel, $sPlugin);
    }

    /**
     * function moved to \Hive\HiveUserfuncs
     * keep this  function for backward compatibility
     *
     * @param $sModel
     * @param $sPlugin
     * @return string
     */
    public static function getFirstStoragePidForModelInPlugin($sModel, $sPlugin)
    {
        return \Hive\HiveUserfuncs\UserFunc\StorageUserFunc::getFirstStoragePidForModelInPlugin($sModel, $sPlugin);
    }
}